package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import javafx.scene.control.Alert;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Notifications {

    private static final Logger log = LogManager.getLogger(Notifications.class);

    public static void showError(String msg, Exception e) {
        log.warn(msg, e);
        if (e instanceof SalesSystemException) {
            msg += ":\n" + e.getMessage();
        }
        showAlert(msg);
    }

    private static void showAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
