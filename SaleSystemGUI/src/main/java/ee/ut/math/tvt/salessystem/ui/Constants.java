package ee.ut.math.tvt.salessystem.ui;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Constants {

    public static final Locale LOCALE = Locale.forLanguageTag("et-EE");

    public static final ZoneOffset ZONE_OFFSET = OffsetDateTime.now().getOffset();

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
            .withLocale(LOCALE)
            .withZone(ZONE_OFFSET);
}
