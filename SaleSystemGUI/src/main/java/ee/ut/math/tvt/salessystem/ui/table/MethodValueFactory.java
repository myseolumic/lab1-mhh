package ee.ut.math.tvt.salessystem.ui.table;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.stream.Stream;

public class MethodValueFactory<E> implements Callback<TableColumn.CellDataFeatures<E, Object>, ObservableValue<Object>> {

    private static final Logger log = LogManager.getLogger(MethodValueFactory.class);

    private String methodName;

    @Override
    public ObservableValue<Object> call(TableColumn.CellDataFeatures<E, Object> param) {
        E value = param.getValue();
        Object result;

        Method method = Stream.of(value.getClass().getDeclaredMethods())
                .filter(m -> m.getName().equals(methodName))
                .findFirst().orElseThrow(() -> new RuntimeException("No method found with name: " + methodName));

        try {
            result = method.invoke(value);
        } catch (Exception e) {
            log.error("Failed to invoke method", e);
            result = "Error";
        }

        return new ReadOnlyObjectWrapper<>(result);
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}