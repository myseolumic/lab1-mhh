package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.PropertiesReader;
import ee.ut.math.tvt.salessystem.ui.Notifications;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    private final SalesSystemDAO dao;

    @FXML
    private Label teamName;
    @FXML
    private Label teamContactPerson;
    @FXML
    private Label teamMembers;
    @FXML
    private ImageView teamImage;

    private Properties teamProp;

    public TeamController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            PropertiesReader propertiesReader = new PropertiesReader();
            teamProp = propertiesReader.getProperties();
            refreshInfo();
        } catch (SalesSystemException e) {
            throw new RuntimeException(e);
        }

    }

    private void refreshInfo() throws SalesSystemException {
        try {
            teamName.setText("Team name: " + teamProp.getProperty("teamName"));
            teamContactPerson.setText("Team contact person: " + teamProp.getProperty("teamContactPerson"));
            teamMembers.setText("Team members: " + teamProp.getProperty("teamMembers"));
            teamImage.setImage(new Image(teamProp.getProperty("logo")));
        } catch (Exception e) {
            throw new SalesSystemException("Failed to refresh team info", e);
        }
    }

    @FXML
    public void editTeamName() {
        try {
            TextInputDialog dialog = new TextInputDialog(teamProp.getProperty("teamName"));
            dialog.setTitle("Edit team name");
            dialog.setHeaderText("Enter new team name");
            dialog.setContentText("New team name:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> teamProp.setProperty("teamName", name));
            refreshInfo();
        } catch (Exception e) {
            Notifications.showError("Failed to edit team name", e);
        }
    }

    @FXML
    public void editContactPerson() {
        try {
            TextInputDialog dialog = new TextInputDialog(teamProp.getProperty("teamContactPerson"));
            dialog.setTitle("Edit team's contact person");
            dialog.setHeaderText("Enter new contact person");
            dialog.setContentText("New contact:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> teamProp.setProperty("teamContactPerson", name));
            refreshInfo();
        } catch (Exception e) {
            Notifications.showError("Failed to edit contact person", e);
        }
    }

    @FXML
    public void editTeamMembers() {
        try {
            TextInputDialog dialog = new TextInputDialog(teamProp.getProperty("teamMembers"));
            dialog.setTitle("Edit team members");
            dialog.setHeaderText("Enter new team members list");
            dialog.setContentText("New members list:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> teamProp.setProperty("teamMembers", name));
            refreshInfo();
        } catch (Exception e) {
            Notifications.showError("Failed to edit team members", e);
        }
    }

    @FXML
    public void editLogoFileName() {
        try {
            TextInputDialog dialog = new TextInputDialog(teamProp.getProperty("logo"));
            dialog.setTitle("Edit team logo");
            dialog.setHeaderText("Enter new team logo file path");
            dialog.setContentText("New file path:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> teamProp.setProperty("logo", name));
            refreshInfo();
        } catch (Exception e) {
            Notifications.showError("Failed to edit logo file name", e);
        }
    }

}
