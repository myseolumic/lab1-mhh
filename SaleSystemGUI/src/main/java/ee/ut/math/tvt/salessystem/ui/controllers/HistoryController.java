package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.ui.Notifications;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.Instant;
import java.util.*;

import static ee.ut.math.tvt.salessystem.ui.Constants.ZONE_OFFSET;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);

    public static final Comparator<Purchase> ITEM_COMPARATOR = Comparator.comparing(Purchase::getInstant).reversed();

    private final SalesSystemDAO dao;

    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;
    @FXML
    private TableView<Purchase> historyTableView;
    @FXML
    private TitledPane purchaseHistoryTitledPane;
    @FXML
    private TableView<SoldItem> shoppingCartTableView;

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showItems(dao.findPurchases());
    }

    @FXML
    public void showAllItems() {
        try {
            showItems(dao.findPurchases());
            log.info("Showing all purchases");
        } catch (Exception e) {
            Notifications.showError("Failed to show all purchases", e);
        }
    }

    @FXML
    public void showLatestItems() {
        try {
            showItems(dao.findPurchases(10));
            log.info("Showing latest purchases");
        } catch (Exception e) {
            Notifications.showError("Failed to show latest purchases", e);
        }
    }

    @FXML
    public void showItemsInRange() {
        try {
            if (startDatePicker.getValue() == null) {
                return;
            }
            if (endDatePicker.getValue() == null || endDatePicker.getValue().isBefore(startDatePicker.getValue())) {
                endDatePicker.setValue(startDatePicker.getValue());
            }
            Instant start = startDatePicker.getValue().atStartOfDay(ZONE_OFFSET).toInstant();
            Instant end = endDatePicker.getValue().atStartOfDay(ZONE_OFFSET).plusDays(1).toInstant();
            showItems(dao.findPurchases(start, end));
            log.info("Showing purchases in range: " + startDatePicker.getValue() + "..." + endDatePicker.getValue());
        } catch (Exception e) {
            Notifications.showError("Failed to show purchases in given range", e);
        }
    }

    @FXML
    public void onSelectedPurchase() {
        try {
            Collection<SoldItem> cart = Optional.of(historyTableView)
                    .map(TableView::getSelectionModel)
                    .map(SelectionModel::getSelectedItem)
                    .map(Purchase::getItems)
                    .orElse(Collections.emptyList());
            showCart(cart);

            Long id = Optional.of(historyTableView)
                    .map(TableView::getSelectionModel)
                    .map(SelectionModel::getSelectedItem)
                    .map(Purchase::getId)
                    .orElse(null);
            log.info("Showing cart for purchase: " + id);
        } catch (Exception e) {
            Notifications.showError("Failed to show cart for selected purchase", e);
        }
    }

    private void showItems(List<Purchase> items) {
        ObservableList<Purchase> itemsObservable = items.stream()
                .sorted(ITEM_COMPARATOR)
                .collect(collectingAndThen(toList(), ObservableListWrapper::new));
        historyTableView.setItems(itemsObservable);
        historyTableView.refresh();

        purchaseHistoryTitledPane.setText(String.format("Purchase history (showing %s rows)", items.size()));
        showCart(Collections.emptyList());
    }

    private void showCart(Collection<SoldItem> items) {
        shoppingCartTableView.setItems(FXCollections.observableArrayList(items));
        shoppingCartTableView.refresh();
    }

}
