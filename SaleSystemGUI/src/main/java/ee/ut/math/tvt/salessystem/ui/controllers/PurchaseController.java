package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.Notifications;
import ee.ut.math.tvt.salessystem.ui.table.CartItemDeletionEvent;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

import static java.util.Optional.ofNullable;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ComboBox<StockItem> nameComboBox;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private Button removeItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private TitledPane shoppingCartTitledPane;

    @FXML
    private ChoiceBox<String> products;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        disableProductField(true);
        resetProductField();
        onCartChanged();

        this.nameComboBox.valueProperty().addListener((observable, oldValue, newValue) -> fillInputsBySelectedStockItem(newValue));
    }

    private void refreshStockItems() {
        this.nameComboBox.setItems(FXCollections.observableList(dao.findStockItems()));
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        try {
            log.info("New sale process started");
            enableInputs();
        } catch (Exception e) {
            Notifications.showError("Failed to start new sale process", e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        try {
            log.info("Sale cancelled");
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            onCartChanged();
        } catch (Exception e) {
            Notifications.showError("Failed to cancel sale process", e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        try {
            log.info("Sale complete");
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            onCartChanged();
        } catch (Exception e) {
            Notifications.showError("Failed to submit purchase", e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem(StockItem stockItem) {
        ofNullable(stockItem)
                .map(StockItem::getId)
                .map(String::valueOf)
                .ifPresent(barCodeField::setText);
        ofNullable(stockItem)
                .map(StockItem::getPrice)
                .map(String::valueOf)
                .ifPresent(priceField::setText);
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() throws SalesSystemException {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            throw new SalesSystemException("Failed to parse item barcode", e);
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        try {
            StockItem stockItem = getStockItemByBarcode();
            int quantity = parseItemQuantity();
            shoppingCart.addItem(new SoldItem(stockItem, quantity));
            onCartChanged();
            resetProductField();
        } catch (Exception e) {
            Notifications.showError("Failed to add item", e);
        }
    }

    private int parseItemQuantity() throws SalesSystemException {
        try {
            return Integer.parseInt(quantityField.getText());
        } catch (NumberFormatException e) {
            throw new SalesSystemException("Failed to parse quantity: \"" + quantityField.getText() + "\"");
        }
    }

    /**
     * Remove item from the cart.
     */
    @FXML
    public void removeItemEventHandler(CartItemDeletionEvent event) {
        try {
            SoldItem item = event.getItem();
            shoppingCart.removeItem(item.getStockItemId());
            onCartChanged();
            log.debug("Item removed from cart");
        } catch (Exception e) {
            Notifications.showError("Failed to remove item from cart", e);
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameComboBox.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameComboBox.getSelectionModel().clearSelection();
        priceField.setText("");
        refreshStockItems();
    }

    private void onCartChanged() {
        shoppingCartTitledPane.setText(String.format("Shopping cart: %.2f\u20AC", shoppingCart.getSum()));
        purchaseTableView.setItems(FXCollections.observableArrayList(shoppingCart.getAll()));
        purchaseTableView.refresh();
    }
}
