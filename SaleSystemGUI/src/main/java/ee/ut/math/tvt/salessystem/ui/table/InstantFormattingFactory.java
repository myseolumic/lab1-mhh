package ee.ut.math.tvt.salessystem.ui.table;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.time.Instant;

import static ee.ut.math.tvt.salessystem.ui.Constants.DATE_TIME_FORMATTER;

public class InstantFormattingFactory<E> implements Callback<TableColumn<E, Instant>, TableCell<E, Instant>> {

    @Override
    public TableCell<E, Instant> call(TableColumn<E, Instant> param) {
        return new TableCell<E, Instant>() {
            @Override
            protected void updateItem(Instant item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : DATE_TIME_FORMATTER.format(item));
            }
        };
    }
}