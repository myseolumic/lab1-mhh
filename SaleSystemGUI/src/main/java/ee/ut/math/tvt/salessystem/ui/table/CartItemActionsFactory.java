package ee.ut.math.tvt.salessystem.ui.table;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;

public class CartItemActionsFactory implements Callback<TableColumn<SoldItem, String>, TableCell<SoldItem, String>> {

    private EventHandler<CartItemDeletionEvent> onDelete;

    @Override
    public TableCell<SoldItem, String> call(TableColumn<SoldItem, String> param) {
        return new TableCell<SoldItem, String>() {
            Button deleteButton = new Button("X");

            @Override
            public void updateItem(String ignored, boolean empty) {
                super.updateItem(ignored, empty);
                if (empty) {
                    setGraphic(null);
                    setText(null);
                } else {
                    TableRow<SoldItem> tableRow = getTableRow();
                    SoldItem item = tableRow == null ? null : tableRow.getItem();

                    deleteButton.setOnAction(event -> onDelete.handle(new CartItemDeletionEvent(getIndex(), item)));
                    setGraphic(deleteButton);
                    setText(null);
                }
            }
        };
    }

    public EventHandler<CartItemDeletionEvent> getOnDelete() {
        return onDelete;
    }

    public void setOnDelete(EventHandler<CartItemDeletionEvent> onDelete) {
        this.onDelete = onDelete;
    }
}