package ee.ut.math.tvt.salessystem.ui.table;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;

public class CartItemDeletionEvent extends Event {
    public static final EventType<ActionEvent> DELETION = new EventType<ActionEvent>(Event.ANY, "DELETION");

    private final int index;
    private final SoldItem item;

    public CartItemDeletionEvent(int index, SoldItem item) {
        super(DELETION);
        this.index = index;
        this.item = item;
    }

    public int getIndex() {
        return index;
    }

    public SoldItem getItem() {
        return item;
    }
}
