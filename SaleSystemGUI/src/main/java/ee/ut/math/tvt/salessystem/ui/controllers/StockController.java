package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.Notifications;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);

    //private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    @FXML
    private Button addItem;
    @FXML
    private Button removeItem;
    @FXML
    private Button updateProduct;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField descriptionField;
    @FXML
    private TextField priceField;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.warehouse = new Warehouse(dao);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearInputs();
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        try {
            refreshStockItems();
            log.info("Warehouse refreshed");
        } catch (Exception e) {
            Notifications.showError("Failed to refresh warehouse", e);
        }
    }

    private void refreshStockItems() {
        try {
            warehouseTableView.setItems(new ObservableListWrapper<>(warehouse.getStockItems()));
            warehouseTableView.refresh();
        } catch (Exception e) {
            Notifications.showError("Failed to refresh stock items", e);
        }
    }

    private void clearInputs() {
        quantityField.setText("");
        nameField.setText("");
        descriptionField.setText("");
        priceField.setText("");
    }

    private StockItem getSelectedProduct() {
        return warehouseTableView.getSelectionModel().getSelectedItem();
    }

    @FXML
    public void updateProduct() {
        try {
            warehouse.updateStock(
                    getSelectedProduct(),
                    nameField.getText(),
                    descriptionField.getText(),
                    parsePrice(),
                    parseQuantity()
            );
            clearInputs();
        } catch (Exception e) {
            Notifications.showError("Failed to update product", e);
        } finally {
            refreshStockItems();
        }
    }

    @FXML
    public void addItemButtonClicked() {
        try {
            warehouse.addToStock(
                    nameField.getText(),
                    descriptionField.getText(),
                    parsePrice(),
                    parseQuantity()
            );
            clearInputs();
        } catch (Exception e) {
            Notifications.showError("Adding product to warehouse failed", e);
        } finally {
            refreshStockItems();
        }
    }

    private Double parsePrice() throws SalesSystemException {
        String text = priceField.getText().trim();
        if (text.isEmpty()) {
            return null;
        }
        try {
            double price = Double.parseDouble(text);
            if (price < 0) {
                throw new SalesSystemException("Price can not be negative.");
            }
            return price;
        } catch (NumberFormatException e) {
            throw new SalesSystemException("Failed to parse price: \"" + text + "\"", e);
        }
    }

    private Integer parseQuantity() throws SalesSystemException {
        String text = quantityField.getText().trim();
        if (text.isEmpty()) {
            return null;
        }
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            throw new SalesSystemException("Failed to parse quantity: \"" + text + "\"", e);
        }
    }
}
