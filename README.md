# Team mhh:
1. Gregor Eesmaa
2. K�rt Ilja
3. Karl Kuusik

## Homework 1:
[Wiki page](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Homework 1)

## Homework 2:
[Wiki page](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Homework 2)

[Issues](https://bitbucket.org/myseolumic/lab1-mhh/issues)

[Class diagrams](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Class diagrams)

## Homework 3:
[Wiki page](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Homework 3)

## Homework 4:
[homework-4 tag](https://bitbucket.org/myseolumic/lab1-mhh/commits/tag/homework-4)

[Wiki page](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Homework%204)

## Homework 5:
<Links to the solution>

## Homework 6:
[Functional test plan & test cases](https://drive.google.com/file/d/1ROmCXA5yUiP-FMrcmwSm1Xs8IHvdZprq/view?usp=sharing)

## Homework 7:
[Task 1](https://docs.google.com/spreadsheets/d/1ksl5AZUY8PPo7Rlhn50nqtVAwDLAV7hEFmtten9BMU8/edit?usp=sharing)

[Task 2.1](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Test%20cases%20results)

[Task 2.2](https://bitbucket.org/myseolumic/lab1-mhh/wiki/Foreign%20usability%20test%20report)

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)