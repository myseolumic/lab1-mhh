package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private static final Logger log = LogManager.getLogger(Console.class);

    private static final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void say(String msg) {
        System.out.println(msg);
        log.info(msg);
    }

    public static void error(String msg) {
        System.err.println(msg);
        log.warn(msg);
    }

    public static void error(String msg, Exception e) {
        log.warn(msg, e);
        if (e instanceof SalesSystemException) {
            msg += ": " + e.getMessage();
        }
        System.err.println(msg);
    }

    public static String ask(String question) throws IOException {
        System.out.print(question);
        String response = in.readLine().trim();
        log.info("{} `{}`", question, response);
        return response;
    }
}
