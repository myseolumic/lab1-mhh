package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.PropertiesReader;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final Warehouse warehouse;

    PropertiesReader propertiesReader;
    Properties prop;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        this.cart = new ShoppingCart(dao);
        this.warehouse = new Warehouse(dao);

        propertiesReader = new PropertiesReader();
        prop = propertiesReader.getProperties();
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        Console.say("===========================");
        Console.say("=       Sales System      =");
        Console.say("===========================");
        printUsage();
        while (true) {
            processCommand(Console.ask(">"));
            Console.say("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        Console.say("-------------------------");
        for (StockItem si : stockItems) {
            Console.say(si.getId() + " " + si.getName() + " " + si.getPrice() + "€ (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            Console.say("\tNothing");
        }
        Console.say("-------------------------");
    }

    private void showCart() {
        Console.say("-------------------------");
        for (SoldItem si : cart.getAll()) {
            Console.say(si.getNameWhenSold() + " " + si.getPriceWhenSold() + "€ x " + si.getQuantity() + " = " + si.getSum() + "€");
        }
        if (cart.getAll().size() == 0) {
            Console.say("\tNothing");
        } else {
            Console.say("\nTotal: " + cart.getSum() + "€");
        }
        Console.say("-------------------------");
    }

    private void showTeam() {
        Console.say("-------------------------");
        Console.say("Team name: " + prop.getProperty("teamName"));
        Console.say("Team contact person: " + prop.getProperty("teamContactPerson"));
        Console.say("Team members: " + prop.getProperty("teamMembers"));
        Console.say("-------------------------");
    }

    private void showHistory(int amount) {
        List<Purchase> items;
        if (amount == 0) {
            items = dao.findPurchases();
        } else {
            items = dao.findPurchases(amount);
        }
        Console.say("-------------------------");
        Console.say("Purchases (id, date and time, sum of items): \n");

        if (items.size() == 0) {
            Console.say("No purchases.");
        } else {
            Console.say("Total number of purchases: " + items.size());
            for (Purchase pi : items) {
                Console.say(pi.getId() + "\t" + pi.getInstant() + "\t" + pi.getSum() + "€");
            }
        }

        Console.say("-------------------------");
    }

    private void showHistory(String start, String end) {
        Console.say("-------------------------");
        List<Purchase> plist = dao.findPurchases(Instant.parse(start.toUpperCase()), Instant.parse(end.toUpperCase()));
        Console.say("Showing purchase history from " + start + " to " + end + ": \n");
        Console.say("Total number of purchases: " + plist.size());
        for (int i = plist.size() - 1; i >= 0; i--) {
            Purchase pi = plist.get(i);
            Console.say(pi.getId() + "\t" + pi.getInstant() + "\t" + pi.getSum() + "€");
        }
        Console.say("-------------------------");
    }

    private void showHistoryItemContents(long id) {
        Console.say("-------------------------");
        Purchase pi = dao.findPurchaseItem(id);
        if (pi == null) {
            Console.error("Purchase with id: " + id + " not found");
            return;
        }
        Collection<SoldItem> items = pi.getItems();
        Console.say("Showing bought items for purchase #" + id + ": \n");
        for (SoldItem item : items) {
            Console.say(item.getId() + " " + item.getNameWhenSold() + " " + item.getPriceWhenSold() + "€ x " + item.getQuantity() + " = " + item.getSum() + "€");
        }
        Console.say("\nTotal: " + pi.getSum() + "€");
        Console.say("Purchase time: " + pi.getInstant());
        Console.say("-------------------------");
    }

    private void updateWarehouseItem(long inx, String attr, String val) {
        try {
            warehouse.updateStock(
                    inx,
                    attr.equals("name") ? val : null,
                    attr.equals("description") ? val : null,
                    attr.equals("price") ? Double.parseDouble(val) : null,
                    attr.equals("+quantity") ? Integer.parseInt(val) : null
            );
        } catch (Exception e) {
            Console.error("Failed to update item in warehouse", e);
        }
    }

    private void addItemToWarehouse() {
        try {
            warehouse.addToStock(
                    Console.ask("Insert new item name:"),
                    Console.ask("Insert new item description:"),
                    Double.parseDouble(Console.ask("Insert new item price:")),
                    Integer.parseInt(Console.ask("Insert new item quantity:"))
            );
        } catch (Exception e) {
            Console.error("Failed to add new item to warehouse", e);
        }
    }

    private void editTeamInfo(String attr, String val) {
        prop.setProperty(attr, val);
    }

    private void printUsage() {
        Console.say("-------------------------");
        Console.say("Usage:");
        Console.say("h\t\tShow this help");
        Console.say("o\t\tShow purchase history");
        Console.say("o 10\t\tShow last 10 purchases in purchase history");
        Console.say("o START END\t\tShow purchases from Instant START to Instant END in purchase history (format: 2018-11-05T17:04:30.310Z)");
        Console.say("i ID\t\tShow more information for purchase #ID");
        Console.say("t\t\tShow team information");
        Console.say("t ATTR VAL\t\tChange team attribute ATTR to new value VAL and shows the new team information");
        Console.say("w\t\tShow warehouse contents");
        Console.say("w a\t\tStarts the process of adding an item to warehouse.");
        Console.say("w INX ATTR VAL\t\tChange warehouse item attribute ATTR(name, description, price, +quantity) on index INX to value VAL and show warehouse contents");
        Console.say("c\t\tShow cart contents");
        Console.say("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        Console.say("a NAME NR \tAdd NR of stock item with name NAME to the cart");
        Console.say("p\t\tPurchase the shopping cart");
        Console.say("r IDX\t\tRemove item with cart index IDX (starting from 0) from shopping cart");
        Console.say("r\t\tReset the shopping cart");
        Console.say("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w")) {
            if (c.length == 2)
                addItemToWarehouse();
            if (c.length == 4) {
                try {
                    updateWarehouseItem(Long.parseLong(c[1]), c[2], c[3]);
                } catch (NumberFormatException e) {
                    Console.error("Failed to parse INX", e);
                }
            }
            showStock();
        } else if (c[0].equals("o")) {
            if (c.length == 3) {
                showHistory(c[1], c[2]);
            } else {
                int amount = 0;
                if (c.length == 2) {
                    try {
                        amount = Integer.parseInt(c[1]);
                    } catch (NumberFormatException e) {
                        Console.error("bad amount, showing all", e);
                    }
                }
                showHistory(amount);
            }
        } else if (c[0].equals("i") && c.length == 2) {
            long id;
            try {
                id = Long.parseLong(c[1]);
            } catch (NumberFormatException e) {
                Console.error("bad ID", e);
                return;
            }
            showHistoryItemContents(id);
        } else if (c[0].equals("t")) {
            if (c.length == 3) {
                editTeamInfo(c[1], c[2]);
            }
            showTeam();
        } else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p")) {
            try {
                cart.submitCurrentPurchase();
            } catch (SalesSystemException e) {
                Console.error("Failed to submit current purchase", e);
            }
        } else if (c[0].equals("r") && c.length == 2) {
            int cartIdx;
            try {
                cartIdx = Integer.parseInt(c[1]);
            } catch (NumberFormatException e) {
                Console.error("bad index", e);
                return;
            }
            if (cartIdx > cart.getAll().size() - 1) {
                Console.error("Index is too big");
                return;
            } else if (cartIdx < 0) {
                Console.error("Index is negative");
                return;
            }
            cart.removeIndex(cartIdx);
        } else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            try {
                int amount = Integer.parseInt(c[2]);
                StockItem item;
                try {
                    item = dao.findStockItem(Long.parseLong(c[1]));
                } catch (NumberFormatException e) {
                    item = dao.findStockItem(c[1].replaceFirst(String.valueOf(c[1].charAt(0)), String.valueOf(c[1].charAt(0)).toUpperCase()));
                }

                if (item != null) {
                    cart.addItem(new SoldItem(item, amount));
                } else {
                    Console.say("no stock item with this name or index");
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                Console.error("Failed to add item to cart", e);
            }
        } else {
            Console.say("unknown command");
        }
    }

}
