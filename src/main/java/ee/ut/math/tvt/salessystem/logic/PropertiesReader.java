package ee.ut.math.tvt.salessystem.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertiesReader {

    private static final Logger log = LogManager.getLogger(PropertiesReader.class);

    Properties teamProp;

    public PropertiesReader() {
        teamProp = new Properties();
        ClassLoader classLoader = getClass().getClassLoader();
        try (BufferedReader reader =
                     Files.newBufferedReader(
                             Paths.get(classLoader.getResource("application.properties").toURI()),
                             Charset.forName("UTF-8")
                     )) {
            teamProp.load(reader);
        } catch (IOException | URISyntaxException e) {
            log.error("Failed to load properties", e);
        }
    }

    public Properties getProperties() {
        return teamProp;
    }

}
