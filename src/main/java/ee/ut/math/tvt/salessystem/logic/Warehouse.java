package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

public class Warehouse {

    private static final Logger log = LogManager.getLogger(Warehouse.class);

    private final SalesSystemDAO dao;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<StockItem> getStockItems() {
        return dao.findStockItems();
    }

    public void updateStock(long id, String name, String description, Double price, Integer quantity) throws SalesSystemException {
        updateStock(dao.findStockItem(id), name, description, price, quantity);
    }

    public void updateStock(StockItem item, String name, String description, Double price, Integer quantity) throws SalesSystemException {
        try {
            dao.beginTransaction();
            if (item == null) {
                throw new SalesSystemException("You must select an item");
            }
            name = Strings.trimToNull(name);
            description = Strings.trimToNull(description);
            if (name != null) {
                log.info("Setting name to \"{}\" for {}", name, item);
                item.setName(name);
            }
            if (description != null) {
                log.info("Setting description to \"{}\" for {}", description, item);
                item.setDescription(description);
            }
            if (price != null) {
                log.info("Setting price to {} for {}", price, item);
                item.setPrice(price);
            }
            if (quantity != null) {
                int newQuantity = item.getQuantity() + quantity;
                if (quantity < 0) {
                    throw new SalesSystemException("Quanitity can not be negative.");
                }
                log.info("Modifying quantity to {}", newQuantity);
                item.setQuantity(newQuantity);
            }
            dao.saveStockItem(item);
            log.info("Saved product in warehouse");
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void addToStock(String name, String description, Double price, Integer quantity) throws SalesSystemException {
        try {
            dao.beginTransaction();
            name = Strings.trimToNull(name);
            description = Strings.trimToNull(description);
            if (name == null) {
                throw new SalesSystemException("Name must be set");
            }
            if (price == null) {
                throw new SalesSystemException("Price must be set");
            }
            if (quantity == null) {
                throw new SalesSystemException("Quantity must be set");
            } else if (quantity < 0) {
                throw new SalesSystemException("Quanitity can not be negative.");
            }
            dao.saveStockItem(new StockItem(name, description, price, quantity));
            log.info("New product added to warehouse");
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }
}
