package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.Instant;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private static final Logger log = LogManager.getLogger(HibernateSalesSystemDAO.class);

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        log.debug("Querying all stock items");
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        log.debug("Querying stock item by id {}", id);
        return em.find(StockItem.class, id);
    }

    @Override
    public StockItem findStockItem(String name) {
        log.debug("Querying stock item by name {}", name);
        return em.createQuery("from StockItem WHERE name=:name", StockItem.class).setParameter("name", name).getSingleResult();
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        log.debug("Persisting stock item {}", stockItem);
        em.persist(stockItem);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        log.debug("Saving sold item {}", item);
        em.persist(item);
    }

    @Override
    public Purchase findPurchaseItem(long id) {
        log.debug("Querying purchase {}", id);
        return em.find(Purchase.class, id);
    }

    @Override
    public void savePurchase(Purchase purchase) {
        log.debug("Persisting purchase {}", purchase);
        em.persist(purchase);
    }

    @Override
    public List<Purchase> findPurchases() {
        log.debug("Querying purchase history");
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }

    @Override
    public List<Purchase> findPurchases(int limit) {
        log.debug("Querying purchase history ({} items)", limit);
        return em.createQuery("from Purchase order by instant desc", Purchase.class)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public List<Purchase> findPurchases(Instant start, Instant end) {
        log.debug("Querying purchase history (in range {} to {})", start, end);
        return em.createQuery("from Purchase where instant between :start and :end", Purchase.class)
                .setParameter("start", start)
                .setParameter("end", end)
                .getResultList();
    }

    @Override
    public void beginTransaction() {
        log.debug("Beginning transaction");
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        log.debug("Rolling back transaction");
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        log.debug("Persisting transaction");
        em.getTransaction().commit();
    }

}
