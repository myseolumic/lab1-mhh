package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.*;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final Map<Long, SoldItem> items = new LinkedHashMap<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) throws SalesSystemException {
        log.debug("Adding item {} to cart", item);
        if (item.getQuantity() <= 0) {
            throw new SalesSystemException("Incorrect quantity!");
        }
        Long key = item.getStockItemId();
        SoldItem newItem = new SoldItem(item.getStockItem(), item.getQuantity());

        int existingQuantity = Optional.ofNullable(items.get(key))
                .map(SoldItem::getQuantity)
                .orElse(0);
        newItem.setQuantity(newItem.getQuantity() + existingQuantity);

        StockItem stockItem = newItem.getStockItem();
        if (newItem.getQuantity() > stockItem.getQuantity()) {
            throw new SalesSystemException("Warehouse quantity exceeded!");
        }
        items.put(key, newItem);
        //log.debug("Added " + newItem.getName() + " quantity of " + newItem.getQuantity());
    }

    public Collection<SoldItem> getAll() {
        return items.values();
    }

    public void cancelCurrentPurchase() {
        log.debug("Cancelling current cart");
        items.clear();
    }

    public void submitCurrentPurchase() throws SalesSystemException {
        log.debug("Submitting current cart");
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        try {
            dao.beginTransaction();
            List<SoldItem> itemsList = new ArrayList<>(getAll());

            Purchase purchase = new Purchase(Instant.now());
            for (SoldItem item : itemsList) {
                item.setPurchase(purchase);
                dao.saveSoldItem(item);
                StockItem stockItem = item.getStockItem();
                stockItem.setQuantity(stockItem.getQuantity() - item.getQuantity());
                dao.saveStockItem(stockItem);
            }
            purchase.setItems(itemsList);
            dao.savePurchase(purchase);
            dao.commitTransaction();

            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw new SalesSystemException("Failed to save shopping cart", e);
        }
    }

    public void removeItem(Long key) {
        log.debug("Removing item with barcode {}", key);
        items.remove(key);
    }

    public void removeIndex(int idx) {
        log.debug("Removing item at index {}", idx);
        List<SoldItem> items = new ArrayList<>(getAll());
        removeItem(items.get(idx).getStockItemId());
    }

    public double getSum() {
        return getAll().stream()
                .mapToDouble(SoldItem::getSum)
                .sum();
    }
}
