package ee.ut.math.tvt.salessystem.dataobjects;


import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
@Table(name = "SOLDITEM")
public class SoldItem {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "stockitemID")
    private StockItem stockItem;

    @Column(name = "name")
    private String nameWhenSold;

    @Column(name = "price")
    private double priceWhenSold;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "purchaseID")
    private Purchase purchase;

    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, int quantity) {
        this.stockItem = stockItem;
        this.nameWhenSold = stockItem.getName();
        this.priceWhenSold = stockItem.getPrice();
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStockItemId() {
        return stockItem.getId();
    }

    public String getNameWhenSold() {
        return nameWhenSold;
    }

    public void setNameWhenSold(String name) {
        this.nameWhenSold = name;
    }

    public double getPriceWhenSold() {
        return priceWhenSold;
    }

    public void setPriceWhenSold(double price) {
        this.priceWhenSold = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public double getSum() {
        return priceWhenSold * ((double) quantity);
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, stockItemId=%d, nameWhenSold='%s', priceWhenSold=%s, quantity=%s}", id, stockItem.getId(), nameWhenSold, priceWhenSold, quantity);
    }
}
