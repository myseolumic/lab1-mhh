package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "PURCHASE")
public class Purchase {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "instant")
    private Instant instant;

    @OneToMany(mappedBy = "purchase")
    private Collection<SoldItem> items; //items.getAll();

    public Purchase() {
    }

    public Purchase(Instant instant) {
        this.instant = instant;
    }

    public Purchase(Long id, Instant instant, List<SoldItem> items) {
        this(instant);
        this.id = id;
        this.items = items;
    }

    public void setItems(Collection<SoldItem> items) {
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<SoldItem> getItems() {
        return items;
    }

    public Instant getInstant() {
        return instant;
    }

    public double getSum() {
        return items.stream()
                .mapToDouble(SoldItem::getSum)
                .sum();
    }

    @Override
    public String toString() {
        return String.format("Purchase{id=%s, instant=%s, items=%s}", id, instant, items);
    }
}
