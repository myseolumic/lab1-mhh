package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;

    private long purchaseIndexFactory = 0;
    private List<Purchase> purchaseHistory;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchaseHistory = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<Purchase> findPurchases() {
        return purchaseHistory;
    }

    @Override
    public List<Purchase> findPurchases(int limit) {
        return purchaseHistory.stream()
                .filter(purchaseItem -> purchaseItem.getInstant() != null)
                .sorted(Comparator.comparing(Purchase::getInstant).reversed())
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Override
    public List<Purchase> findPurchases(Instant start, Instant end) {
        return purchaseHistory.stream()
                .filter(purchaseItem -> purchaseItem.getInstant() != null)
                .filter(purchaseItem -> !purchaseItem.getInstant().isBefore(start))
                .filter(purchaseItem -> !purchaseItem.getInstant().isAfter(end))
                .collect(Collectors.toList());
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(String input) {
        for (StockItem item : stockItemList) {
            if (item.getName().contains(input)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public Purchase findPurchaseItem(long id) {
        return purchaseHistory.stream()
                .filter(purchaseItem -> purchaseItem.getId() == id)
                .findFirst().orElse(null);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void savePurchase(Purchase purchase) {
        purchase.setId(purchaseIndexFactory++);
        purchaseHistory.add(purchase);
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }

    public void setPurchaseHistory(List<Purchase> purchaseHistory) {
        this.purchaseHistory = purchaseHistory;
    }
}
