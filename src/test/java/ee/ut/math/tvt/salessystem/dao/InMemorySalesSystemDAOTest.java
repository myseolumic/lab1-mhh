package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.Samples;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.LongUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class InMemorySalesSystemDAOTest {

    InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();

    @Test
    void givenPurchaseItem_whenSavingShoppingCart_thenIdAssigned() {
        Purchase purchase = new Purchase(null, Instant.now(), Arrays.asList(Samples.SOLD_ITEM_1, Samples.SOLD_ITEM_2));

        dao.savePurchase(purchase);

        assertNotNull(purchase.getId());
    }

    @Test
    void givenPurchaseItem_whenSavingMultipleShoppingCarts_thenIdsDifferent() {
        Purchase purchase1 = new Purchase(null, Instant.now(), Arrays.asList(Samples.SOLD_ITEM_1, Samples.SOLD_ITEM_2));
        Purchase purchase2 = new Purchase(null, Instant.now(), Arrays.asList(Samples.SOLD_ITEM_1, Samples.SOLD_ITEM_2));
        Purchase purchase3 = new Purchase(null, Instant.now(), Arrays.asList(Samples.SOLD_ITEM_1, Samples.SOLD_ITEM_2));

        dao.savePurchase(purchase1);
        dao.savePurchase(purchase2);
        dao.savePurchase(purchase3);

        assertNotEquals(purchase1.getId(), purchase2.getId());
        assertNotEquals(purchase1.getId(), purchase3.getId());
        assertNotEquals(purchase2.getId(), purchase3.getId());
    }

    @Test
    void givenPurchasesInHistory_whenAllQueried_thenAllReturned() {
        List<Purchase> purchases = generateItems(4);
        dao.setPurchaseHistory(purchases);

        List<Purchase> result = dao.findPurchases();

        assertEquals(4, result.size());
        assertArrayEquals(purchases.toArray(), result.toArray());
    }

    @Test
    void givenPurchasesInHistory_whenLast10Queried_thenLast10Returned() {
        List<Purchase> items = generateItems(10);
        List<Purchase> newItems = generateItems(10, l -> l + 10);
        items.addAll(newItems);
        Collections.shuffle(items); // last items might not really be the last in the list
        dao.setPurchaseHistory(items);

        List<Purchase> result = dao.findPurchases(10);

        assertEquals(10, result.size());
        assertTrue(result.containsAll(newItems));
        assertTrue(newItems.containsAll(result));
    }

    @Test
    void givenPurchasesInHistory_whenRangeQueried_thenRangeReturned() {
        Instant start = Instant.ofEpochMilli(1);
        Instant end = Instant.ofEpochMilli(2);
        List<Purchase> purchases = generateItems(4);
        dao.setPurchaseHistory(purchases);

        List<Purchase> result = dao.findPurchases(start, end);

        assertEquals(2, result.size());
        assertFalse(result.contains(purchases.get(0)));
        assertTrue(result.contains(purchases.get(1)));
        assertTrue(result.contains(purchases.get(2)));
        assertFalse(result.contains(purchases.get(3)));
    }

    @Test
    void givenPurchasesInHistory_whenQueriedByExistingId_thenMatchingPurchaseReturned() {
        List<Purchase> purchases = generateItems(4);
        dao.setPurchaseHistory(purchases);

        Purchase result = dao.findPurchaseItem(2);

        assertEquals(purchases.get(2), result);
    }

    @Test
    void givenPurchasesInHistory_whenQueriedByNonexistentId_thenNullReturned() {
        List<Purchase> purchases = generateItems(4);
        dao.setPurchaseHistory(purchases);

        Purchase result = dao.findPurchaseItem(4);

        assertNull(result);
    }

    private static List<Purchase> generateItems(int n) {
        return generateItems(n, LongUnaryOperator.identity());
    }

    private static List<Purchase> generateItems(int n, LongUnaryOperator indexMapper) {
        return LongStream.range(0, n)
                .map(indexMapper)
                .mapToObj(l -> new Purchase(l, Instant.ofEpochMilli(l), null))
                .collect(Collectors.toList());
    }
}