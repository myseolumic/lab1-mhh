package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

public class Samples {
    public static final StockItem STOCK_ITEM_1 = new StockItem(1L, "rhubarb", "Om nom nom.", 5.0, 7);
    public static final SoldItem SOLD_ITEM_1 = new SoldItem(STOCK_ITEM_1, 1);

    public static final StockItem STOCK_ITEM_2 = new StockItem(2L, "eggs", "These sure make a good omelet.", 2.7, 6);
    public static final SoldItem SOLD_ITEM_2 = new SoldItem(STOCK_ITEM_2, 2);
}
