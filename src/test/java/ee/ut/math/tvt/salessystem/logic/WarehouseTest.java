package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class WarehouseTest {

    InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();

    private SalesSystemDAO daoMock = Mockito.mock(SalesSystemDAO.class);
    private Warehouse warehouse;

    @Test
    void testAddingItemBeginsAndCommitsTransaction() throws SalesSystemException {
        warehouse = new Warehouse(daoMock);
        warehouse.addToStock("cheese", "very smelly",1.2, 1);

        //check methods were called once
        verify(daoMock, times(1)).beginTransaction();
        verify(daoMock, times(1)).commitTransaction();
        //check order
        InOrder inOrder = Mockito.inOrder(daoMock);
        inOrder.verify(daoMock).beginTransaction();
        inOrder.verify(daoMock).commitTransaction();
    }

    @Test
    void testAddingNewItem() throws SalesSystemException{
        warehouse = new Warehouse(dao);
        warehouse.addToStock("cheese", "very smelly",1.2, 1);
        assertNotNull(dao.findStockItem("cheese"));
    }

    @Test
    void testAddingExistingItem() throws SalesSystemException{
        StockItem dummy = new StockItem("cheese", "very smelly",1.2, 1);
        doReturn(dummy).when(daoMock).findStockItem("cheese");

        warehouse = new Warehouse(daoMock);
        warehouse.updateStock(dummy, "cheese", "very smelly", 1.2, 1);

        assertEquals(2, dummy.getQuantity());
        verify(daoMock).saveStockItem(dummy);
    }

    @Test
    void testAddingItemWithNegativeQuantity(){
        warehouse = new Warehouse(daoMock);
        Executable e = () -> warehouse.addToStock("cheese", "very smelly",1.2, -1);
        assertThrows(SalesSystemException.class, e);
    }

}
