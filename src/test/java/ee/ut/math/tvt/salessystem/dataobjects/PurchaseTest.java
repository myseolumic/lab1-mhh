package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.Samples;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PurchaseTest {

    @Test
    void givenTwoItems_whenGetSumCalled_thenCorrectSumReturned() {
        double expectedSum = Samples.SOLD_ITEM_1.getSum() + Samples.SOLD_ITEM_2.getSum();
        Purchase purchase = new Purchase(0L, Instant.now(), Arrays.asList(Samples.SOLD_ITEM_1, Samples.SOLD_ITEM_2));

        double actualSum = purchase.getSum();

        assertNotEquals(0, expectedSum); // just in case
        assertEquals(expectedSum, actualSum);
    }
}