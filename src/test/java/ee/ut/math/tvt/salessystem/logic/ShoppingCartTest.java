package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.Samples;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ShoppingCartTest {

    @Mock
    private SalesSystemDAO dao;

    private ShoppingCart cart;

    @Captor
    ArgumentCaptor<Purchase> purchaseItemCaptor;

    @BeforeEach
    void setUp() {
        cart = new ShoppingCart(dao);
    }

    //addItem
    @Test
    void testAddingExistingItem() throws SalesSystemException{
        addSampleItems();
        List<SoldItem> contents = new ArrayList<>(cart.getAll());
        SoldItem previousItem = contents.get(0);
        int previousQuantity = previousItem.getQuantity();

        addSampleItems();
        contents = new ArrayList<>(cart.getAll());
        SoldItem newItem = contents.get(0);
        int newQuantity = newItem.getQuantity();

        assertEquals(previousQuantity+1, newQuantity);
        assertEquals(previousItem.getId(), newItem.getId());
    }

    @Test
    void testAddingNewItem() throws SalesSystemException{
        cart.addItem(Samples.SOLD_ITEM_1);
        SoldItem item = new ArrayList<>(cart.getAll()).get(0);
        assertEquals(Samples.SOLD_ITEM_1.getStockItemId(), item.getStockItemId());
    }

    @Test //still needs to detect the exception
    void testAddingItemWithNegativeQuantity(){
        Executable e = () -> cart.addItem(new SoldItem(Samples.STOCK_ITEM_1, -1));
        assertThrows(SalesSystemException.class, e);
    }

    @Test //still needs to detect the exception
    void testAddingItemWithQuantityTooLarge(){
        Executable e = () -> cart.addItem(new SoldItem(Samples.STOCK_ITEM_1, 20));
        assertThrows(SalesSystemException.class, e);
    }

    @Test
    void testAddingItemWithQuantitySumTooLarge() throws SalesSystemException{
        cart.addItem(new SoldItem(Samples.STOCK_ITEM_1, 7));
        Executable e = () -> cart.addItem(new SoldItem(Samples.STOCK_ITEM_1, 1));
        assertThrows(SalesSystemException.class, e);
    }

    //submitCurrentPurchase
    @Test
    void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() throws SalesSystemException{
        dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);

        int before = dao.findStockItem("Frankfurters").getQuantity();
        cart.addItem(new SoldItem(dao.findStockItem("Frankfurters"),1));
        cart.submitCurrentPurchase();
        int after = dao.findStockItem("Frankfurters").getQuantity();

        assertTrue(before > after);
    }

    @Test
    void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() throws SalesSystemException{
        addSampleItems();
        cart.submitCurrentPurchase();

        //check methods were called once
        verify(dao, times(1)).beginTransaction();
        verify(dao, times(1)).commitTransaction();
        //check order
        InOrder inOrder = Mockito.inOrder(dao);
        inOrder.verify(dao).beginTransaction();
        inOrder.verify(dao).commitTransaction();

    }

    @Test
    void testSubmittingCurrentOrderCreatesHistoryItem() throws SalesSystemException{
        dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);
        addSampleItems();
        cart.submitCurrentPurchase();
        List<Purchase> purchases = dao.findPurchases();
        assertEquals(1, purchases.size());
    }

    @Test
    void testSubmittingCurrentOrderSavesCorrectTime()throws SalesSystemException{
        dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);
        addSampleItems();
        Instant now = Instant.now();
        cart.submitCurrentPurchase();
        List<Purchase> purchases = dao.findPurchases();
        Duration diff = Duration.between(now,purchases.get(0).getInstant());
        assertTrue(diff.getNano() < 10); // i think 10 nanoseconds will suffice.
    }

    @Test
    void testCancellingOrder() throws SalesSystemException{
        addSampleItems();
        cart.cancelCurrentPurchase();
        assertEquals(0, cart.getAll().size());
    }

    @Test
    void testCancellingOrderQuanititesUnchanged() throws SalesSystemException{
        dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);

        int before = dao.findStockItem("Frankfurters").getQuantity();
        cart.addItem(new SoldItem(dao.findStockItem("Frankfurters"),1));
        cart.cancelCurrentPurchase();
        int after = dao.findStockItem("Frankfurters").getQuantity();

        assertEquals(before, after);
    }


    //misc
    @Test
    void givenEmptyCart_whenAddItemCalledMultipleTimesWithDifferentItems_thenItemsAdded() throws SalesSystemException {
        addSampleItems();

        Collection<SoldItem> result = cart.getAll();
        assertEquals(2, result.size());
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_1.getStockItemId().equals(i.getStockItemId())));
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_2.getStockItemId().equals(i.getStockItemId())));
    }

    @Test
    void givenEmptyCart_whenAddItemCalledMultipleTimesWithSameItem_thenOnlyOneItemAdded() throws SalesSystemException {
        cart.addItem(Samples.SOLD_ITEM_1);
        cart.addItem(Samples.SOLD_ITEM_1);

        Collection<SoldItem> result = cart.getAll();
        assertEquals(1, result.size());
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_1.getStockItemId().equals(i.getStockItemId())));
        //noinspection ConstantConditions
        assertEquals(2 * Samples.SOLD_ITEM_1.getQuantity(), (int) result.stream().findAny().get().getQuantity());
    }

    @Test
    void givenCartWithItems_whenRemoveItemCalled_thenItemRemoved() throws SalesSystemException {
        addSampleItems();

        cart.removeItem(Samples.SOLD_ITEM_1.getStockItemId());

        Collection<SoldItem> result = cart.getAll();
        assertEquals(1, result.size());
        assertFalse(result.stream().anyMatch(i -> Samples.SOLD_ITEM_1.getStockItemId().equals(i.getStockItemId())));
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_2.getStockItemId().equals(i.getStockItemId())));
    }

    @Test
    void givenCartWithItems_whenGetSumCalled_thenSumReturned() throws SalesSystemException {
        double expectedSum = Samples.SOLD_ITEM_1.getSum() + Samples.SOLD_ITEM_2.getSum();
        addSampleItems();

        double actualSum = cart.getSum();

        assertNotEquals(0, expectedSum); // just in case
        assertEquals(expectedSum, actualSum);
    }

    @Test
    void givenCartWithoutItems_whenGetSumCalled_thenZeroReturned() {
        double sum = cart.getSum();

        assertEquals(0, sum);
    }

    @Test
    void givenCartWithItems_whenPurchaseSubmitted_thenShoppingCartSaved() throws SalesSystemException {
        addSampleItems();

        Instant before = Instant.now();
        cart.submitCurrentPurchase();
        Instant after = Instant.now();

        verify(dao).savePurchase(purchaseItemCaptor.capture());
        Purchase purchase = purchaseItemCaptor.getValue();
        assertNull(purchase.getId()); // ID should not be assigned there.
        assertInstantBetween(before, after, purchase.getInstant());
        Collection<SoldItem> result = purchase.getItems();
        assertEquals(2, result.size());
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_1.getStockItemId().equals(i.getStockItemId())));
        assertTrue(result.stream().anyMatch(i -> Samples.SOLD_ITEM_2.getStockItemId().equals(i.getStockItemId())));

    }

    void addSampleItems() throws SalesSystemException {
        cart.addItem(Samples.SOLD_ITEM_1);
        cart.addItem(Samples.SOLD_ITEM_2);
    }

    static void assertInstantBetween(Instant start, Instant end, Instant actual) {
        assertFalse(actual.isBefore(start));
        assertFalse(actual.isAfter(end));
    }
}